--[[--------------------------------------------------------------------------
Copyright (c) 2019 Slaynot

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
--]]--------------------------------------------------------------------------

effectToggler = {}

--[[--------------------------------------------------------------------------
Description:
  Lauched at startup.
  Set the macro button according to the current setting
]]----------------------------------------------------------------------------
function effectToggler.Initialize()

  effectToggler.SELF = 2
  effectToggler.ALL = 5

  effectToggler.macroName = L"effectToggler"
  effectToggler.macroCommand = L"/script effectToggler.Toggle()"

  effectToggler.iconOn = 407
  effectToggler.iconOff = 408

  -- Getting the current setting slot
  local currentCustomSettingNumber = EA_Window_CustomizePerformance.CurrCustom
  local settingValue = SystemData.Settings.Performance["custom"..currentCustomSettingNumber]["abilityEffects"]

  if settingValue == effectToggler.ALL then

    effectToggler.toggleMacroSlot = effectToggler.CreateMacro(effectToggler.macroName, effectToggler.macroCommand, effectToggler.iconOn)

  else

    effectToggler.toggleMacroSlot = effectToggler.CreateMacro(effectToggler.macroName, effectToggler.macroCommand, effectToggler.iconOff)

  end
end

--[[--------------------------------------------------------------------------
Description:
  Called when the macro button is clicked.
  Swap the 'play ability effects on' option between 'self' and 'all'
  Update the macro button according to the new settings.
]]----------------------------------------------------------------------------
function effectToggler.Toggle()

  -- Getting the current setting slot
  local currentCustomSettingNumber = EA_Window_CustomizePerformance.CurrCustom
  local settingValue = SystemData.Settings.Performance["custom"..currentCustomSettingNumber]["abilityEffects"]

  -- Check the current settings and swap accordingly
  if settingValue == effectToggler.ALL then

    SystemData.Settings.Performance["custom"..currentCustomSettingNumber]["abilityEffects"] = effectToggler.SELF

    effectToggler.toggleMacroSlot = effectToggler.CreateMacro(effectToggler.macroName, effectToggler.macroCommand, effectToggler.iconOff)

  else

    SystemData.Settings.Performance["custom"..currentCustomSettingNumber]["abilityEffects"] = effectToggler.ALL

    effectToggler.toggleMacroSlot = effectToggler.CreateMacro(effectToggler.macroName, effectToggler.macroCommand, effectToggler.iconOn)

  end

  -- Notify the game that we changed a setting
  BroadcastEvent( SystemData.Events.USER_SETTINGS_CHANGED )
end

--[[--------------------------------------------------------------------------
Description:
  Set a macro based on the parameters
Parameters:
  number slot
    The number of the macro slot
  string name
    The name of the macro
  string text
    The command executed by the macro button
  number iconId
    The id of the macro icon
]]----------------------------------------------------------------------------
function effectToggler.SetMacro(slot, name, text, iconId)

	SetMacroData(name, text, iconId, slot)
	EA_Window_Macro.UpdateDetails(slot)

end

--[[--------------------------------------------------------------------------
Description:
  Create the addon macro if it does not already exists. If so, just update it
Parameters:
  string name
    The name of the macro
  string text
    The command executed by the macro button
  number iconId
    The id of the macro icon
Return:
  number slot
    The number of the macro slot or nil of there is no slot left
]]----------------------------------------------------------------------------
function effectToggler.CreateMacro(name, text, iconId)

	local slot = effectToggler.GetMacroSlot(name)

	if slot then
		effectToggler.SetMacro(slot, name, text, iconId)
		return slot
	end

  -- If there is no macro yet, search for an empty macro slot and create it
	local macros = DataUtils.GetMacros()

	for slot = 1, EA_Window_Macro.NUM_MACROS do
		if macros[slot].name == L"" then
			effectToggler.SetMacro(slot, name, text, iconId)
			return slot
		end
	end

	return nil

end

--[[--------------------------------------------------------------------------
Description:
  Search for the slot containing the addon's macro
Parameters:
  string name
    The name of the macro
Return:
  number slot
    The number of the macro slot or nil there is no macro by this name
]]----------------------------------------------------------------------------
function effectToggler.GetMacroSlot(name)

	local macros = DataUtils.GetMacros()

	for slot = 1, EA_Window_Macro.NUM_MACROS do
		if macros[slot].name == name then
			return slot
		end
	end

	return nil

end
