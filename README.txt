﻿Dissector

LICENSE

This addon is released under the MIT License, see LICENSE.txt for more details.

DESCRIPTION

Add a macro button that allows you to switch the 'play ability effects on' option between 'self' and 'all' without going to the settings.
